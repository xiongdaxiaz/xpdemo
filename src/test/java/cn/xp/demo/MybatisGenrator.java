package cn.xp.demo;

import com.baomidou.mybatisplus.generator.config.PackageConfig;
//import com.github.wujiuye.mybatisplus.generator.EasyMybatisGenerator;

/**
 * @Description: TODO
 * @Author: xp
 * @CreateDate: 2020/11/18 14:08
 * @UpdateUser: 熊鹏
 * @Version: 1.0
 */
public class MybatisGenrator {
    public static void main(String[] args) throws Exception {
        // 配置包信息
        PackageConfig config = new PackageConfig()
                .setParent("cn.richinfo.mall")
                .setEntity("orm.model")
                .setMapper("orm.mapper")
                .setXml("mapper");
        // 开始生成代码
//        EasyMybatisGenerator.run(config,"user");

    }
}
