package cn.xp.demo;

import java.util.Scanner;

/**
 * @Description: TODO
 * @Author: xp
 * @CreateDate: 2021/4/7 14:54
 * @UpdateUser: 熊鹏
 * @Version: 1.0
 */
public class PlantTreesTest {
    public static void main(String[] args) {
        int days;
        double sum = 0;
        long energy = 38570;
        System.out.println("需要种植山杏树："+energy+"g");
        System.out.print("请输入种植人数:");
        Scanner input=new Scanner(System.in);
        int peopleNum =  input.nextInt();

        for (int i=1;i<=peopleNum;i++){
            System.out.print("请输入"+i+"号的步数：");
            Scanner in=new Scanner(System.in);
            long steps = in.nextInt();
            double t = steps/60;
            if (t>=296){
                t= 296;
            }
            sum = sum+t;
        }
        days = (int) (energy/sum);
        if (days!=0){
            days = days+1;
        }
        System.out.println("计算天数"+days);
    }
}
