//package cn.xp.demo;
//
//import cn.xp.demo.component.rabbitmq.HelloReceiver;
//import cn.xp.demo.component.rabbitmq.HelloSender;
//import cn.xp.demo.component.rabbitmq.NeoSender;
//import cn.xp.demo.component.rabbitmq.NeoSender2;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
///**
// * @Description: TODO
// * @Author: xp
// * @CreateDate: 2021/1/23 16:43
// * @UpdateUser: 熊鹏
// * @Version: 1.0
// */
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class RabbitMqTest {
//    @Autowired
//    private HelloSender helloSender;
//
//    private HelloReceiver helloReceiver;
//
//    @Test
//    public void hello() throws Exception {
//        helloSender.send();
//    }
//
//    @Test
//    public void helloReceiver() throws Exception {
//        helloReceiver.process("11");
//    }
//    @Autowired
//    private NeoSender neoSender;
//
//    @Autowired
//    private NeoSender2 neoSender2;
//
//    @Test
//    public void oneToMany() throws Exception {
//        for (int i=0;i<100;i++){
//            neoSender.send(i);
//        }
//    }
//
//    @Test
//    public void manyToMany() throws Exception {
//        for (int i=0;i<100;i++){
//            neoSender.send(i);
//            neoSender2.send(i);
//        }
//    }
//}
