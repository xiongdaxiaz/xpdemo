package cn.xp.demo;

import cn.xp.demo.orm.mapper.BusinessTemplateInfoMapper;
import cn.xp.demo.orm.model.entity.BusinessTemplateInfo;
import cn.xp.demo.service.IBusinessTemplateInfoService;
import cn.xp.demo.service.ITpShorturlInfoService;
import cn.xp.demo.service.RedisService;
import cn.xp.demo.service.impl.*;
import cn.xp.demo.service.impl.pay.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;


import java.io.IOException;
import java.io.StringWriter;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Description: TODO
 * @Author: xp
 * @CreateDate: 2021/1/14 16:49
 * @UpdateUser: 熊鹏
 * @Version: 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest()
@Slf4j
public class testPay {
    @Autowired
    private PayService2 service2;
    @Autowired
    private PayService3 service3;
    @Autowired
    private PayService4 service4;
    @Autowired
    private PayService5 service5;
    @Autowired
    private PayHandlerChain payHandlerChain;
    @Autowired
    private  MsgContentConversionService msgContentConversionService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private IBusinessTemplateInfoService businessTemplateInfoService;
    @Autowired
    private BusinessTemplateInfoMapper templateInfoMapper;

    @Test
    public void testPay(){
//        service2.pay("weixin");
//        service3.toPay("weixin");
//        service5.toPay("weixin");
//        payHandlerChain.handlePay("weixin");
        int a =32;
        String t  = "key";
        int b = t.hashCode();
        System.out.println(b);
        String u  = "keyqweq1";
        int c = t.hashCode();
        Integer t1 = 11;
        int d = t1.hashCode();
        System.out.println(d);
        System.out.println(1<<31);
    }
    @Autowired
    private ITpShorturlInfoService shorturlInfoService;
    @Test
    public  void testCache(){
        shorturlInfoService.updateUrl("0002ddd","https://www.zhihu.com/question/506113303");
        for (int i = 0;i<10;i++){
            long time = System.currentTimeMillis();
            String url = shorturlInfoService.getUrl("0002ddd");
            long endtime = System.currentTimeMillis();
            System.out.println("短链url:"+url);
            System.out.println("第"+i+"次短链耗时:"+(endtime-time));
        }

    }
    @Test
    public void  testMsg(){
        msgContentConversionService.conversion(1);
    }
    @Test
    public void testRedis(){
        String key = MessageFormat.format("{0}:{1}:{2}",  "sys","tableId","template");
            long l = redisService.incr(key,1);
            String value = StringUtils.leftPad(String.valueOf(l), 6, "0");
            System.out.println("模板主键id："+value);

        //incremem
    }
    @Test
    public void testBase62(){
        String key = MessageFormat.format("{0}:{1}:{2}",  "APP","SHORTURL","ID");
//        redisService.set(key,10L);
        long l = redisService.incr(key,1);
        System.out.println(l);
    }
    @Test
    public void testIncrID(){
        BusinessTemplateInfo templateInfo = new BusinessTemplateInfo();
        templateInfo.setCreateTime(LocalDateTime.now());
        templateInfo.setContentId(1l);
        templateInfo.setTemplateName("测试");
        templateInfo.setBusinessBelong(2);
        templateInfoMapper.insert(templateInfo);
//        businessTemplateInfoService.save(templateInfo);
    }

    @Test
    public void testStr(){
        String regex = "\\{param.data}";
        Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
        String content = "测试数据{param.data}";
        Matcher matcher = pattern.matcher(content);
        if (matcher.find()) {
            String result= matcher.group();
            System.out.println(result);
        }
    }

    /**
     *
     * @param templateContent 需要给字符串模板内容,例如"欢迎：${name}登入!"
     * @param
     */
    public static void getPage(String templateContent,Map<String,Object> dataMap) {

        Configuration cfg = new Configuration();
        StringTemplateLoader stringLoader = new StringTemplateLoader();
        stringLoader.putTemplate("myTemplate",templateContent);

        cfg.setTemplateLoader(stringLoader);

        try {
            Template template = cfg.getTemplate("myTemplate","utf-8");
            StringWriter writer = new StringWriter();
            try {
                template.process(dataMap, writer);
                System.out.println(writer.toString());
            } catch (TemplateException e) {
                e.printStackTrace();
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }




}
