package cn.xp.demo;



import cn.xp.demo.common.util.JsonUtil;
import cn.xp.demo.entity.vo.UserHitokotoVo;


import cn.xp.demo.thread.DemoThread;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;




@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ApplicationTests {
    private static  String URL = "https://v1.hitokoto.cn/";

    private static final int CORE_POOL_SIZE = 1;
    private static final int MAX_POOL_SIZE  = 1;
    private static final int QUEUE_CAPACITY = 1;
    private static final Long KEEP_ALIVE_TIME = 1L;
//    @Resource
//    private UserHitokotoMapper userHitokotoMapper;
//    @Autowired
//    private UserHitokotoServiceImpl hitokotoService;
//    @Resource
//    private UserMapper userMapper;
//    @Autowired
//    private UserServiceImpl userService;


    public void testHitoko() throws Exception {
        UserHitokotoVo rspBean = null;

        TypeReference<UserHitokotoVo> ref = new TypeReference<UserHitokotoVo>(){};

        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(URL);
        HttpResponse response = client.execute(httpGet);
        String rspJson = EntityUtils.toString(response.getEntity(), "utf-8");
//        ("一言请求：{}"+rspJson);
        rspBean = JsonUtil.parseToObject(rspJson, ref);
//        UserHitokoto userHitokoto = new UserHitokoto();
//        BeanUtils.copyProperties(rspBean,userHitokoto);
//        userHitokoto.setCreateTime(LocalDateTime.now());
//        userHitokoto.setHitFrom(rspBean.getFrom());
//        userHitokoto.setFromWho(rspBean.getFrom_who());
//        userHitokoto.setCreatorUid(rspBean.getCreator_uid());
//        userHitokoto.setCreatedAt(rspBean.getCreatedAt());
//        userHitokoto.setCommitFrom(rspBean.getCommit_from());
//        hitokotoService.saveOrUpdate(userHitokoto);
    }

    public void testUser(){
//        for (int i = 0; i < 10; i++) {
//            User user = new User();
//            user.setCity("深圳");
//            user.setName("xp3");
//            userMapper.insert(user);
//            System.out.println(user.getId());
//        }
//        List<User> users = userMapper.selectList(new QueryWrapper<User>().select().le("create_time",LocalDateTime.now()));
//        users.forEach(u->
//                System.out.println(u.getName()+u.getCity()));
//
//        userService.saveBatch(users);

//        User u = userService.getById(1323814649320394753l);
//        System.out.println(u.getName());
    }

    public void testFk(){
        Long l = 1323829697547816962l;
        Long i = l>>22;
        System.out.println("-----"+i);
    }

    public void testMysql(){
        for (int i = 1; i < 128; i++) {
            System.out.println("create table msg_user_down_"+i+" like msg_user_down_0;");
        }
        for (int i = 1; i < 128; i++) {
            System.out.println("create table msg_user_up_"+i+" like msg_user_up_0;");
        }
        for (int i = 1; i < 128; i++) {
            System.out.println("DROP TABLE  msg_user_down_"+i+";");
        }
    }


    public static void main(String[] args) {
        for (int i = 1; i < 128; i++) {
            System.out.println("create table msg_user_up_"+i+" like msg_user_up_0;");
        }
    }


    /**
     *
     */

    public void testThreadPool(){
        // 使用线程池来创建线程
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                // 核心线程数为 ：5
                CORE_POOL_SIZE,
                // 最大线程数 ：10
                MAX_POOL_SIZE,
                // 等待时间 ：1L
                KEEP_ALIVE_TIME,
                // 等待时间的单位 ：秒
                TimeUnit.SECONDS,
                // 任务队列为 ArrayBlockingQueue，且容量为 100
                new ArrayBlockingQueue<>(QUEUE_CAPACITY),
                // 饱和策略为 CallerRunsPolicy
                new ThreadPoolExecutor.DiscardOldestPolicy()
        );

        for(int i = 0; i < 10; i++) {
            // 创建WorkerThread对象，该对象需要实现Runnable接口
            Runnable worker = new DemoThread("任务" + i);
            // 通过线程池执行Runnable
            threadPoolExecutor.execute(worker);
        }
        // 终止线程池
        threadPoolExecutor.shutdown();
        while (!threadPoolExecutor.isTerminated()) {

        }
        System.out.println("全部线程已终止");
    }

    public void testSql(){
//        List<UserHitokoto> userHitokotos = userHitokotoMapper.selectList(
//                new QueryWrapper<UserHitokoto>().eq("type","a"));
//        int randomInt = RandomUtil.randomInt(0,userHitokotos.size());
//        UserHitokoto userHitokoto = userHitokotos.get(randomInt);
//        System.out.println("---"+userHitokoto.getHitokoto());
//        UserHitokoto userHitokoto = userHitokotoMapper.selectById(29L);
//        System.out.println("---"+userHitokoto.getHitokoto());
    }



}
