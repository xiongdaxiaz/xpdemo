package cn.xp.demo.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: 消息队列基本配置
 * @Author: xp
 * @CreateDate: 2021/1/23 13:55
 * @UpdateUser: 熊鹏
 * @Version: 1.0
 */
//@Configuration
//public class RabbitConfig {
//    @Bean
//    public Queue Queue() {
//        return new Queue("hello");
//    }
//    @Bean
//    public Queue neoQueue() {
//        return new Queue("neo");
//    }
//
//    @Bean
//    public Queue objectQueue() {
//        return new Queue("object");
//    }
//}
